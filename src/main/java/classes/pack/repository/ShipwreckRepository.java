package classes.pack.repository;

import classes.pack.model.Shipwreck;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Vlad on 7/7/2017.
 */
public interface ShipwreckRepository extends JpaRepository<Shipwreck, Long> {



}
